import json
from os.path import dirname, join
from typing import List
from unittest.mock import mock_open, patch, call

from src.core.config_validation.config import config
from src.core.config_validation.config.config import ConfigFile, ConfigDetails
from src.core.models import Info, Service, Variables, Secrets, RouterInfo
from src.core.setup_services import (
    create_configs,
    write_env_file,
    write_yaml,
    WRITE_PATH,
)


test_root = dirname(dirname(__file__))

mock_vars = {
    "personal_email": "a@b.com",
    "personal_domain": "b.com",
    "namecheap_username": "user",
}

secrets = {"namecheap_api_key": "secretkey", "generate": True}

mock_info = Info(
    list(Service),
    Variables(**mock_vars),
    Secrets(**secrets),  # type: ignore
    RouterInfo.netgear.value.r6300,  # type: ignore
)

json_configs = json.load(open(join(test_root, "fixtures/configs.json")))

mock_config_files: List[ConfigFile] = [
    ConfigFile(filename=f'{c["name"]}.yml', config=c["config"]) for c in json_configs
]


def test_write_env_file() -> None:
    lines = [
        f'PERSONAL_DOMAIN={mock_vars["personal_domain"]}\n',
        f'PERSONAL_EMAIL={mock_vars["personal_email"]}\n',
        f'NAMECHEAP_USERNAME={mock_vars["namecheap_username"]}\n',
    ]
    m = mock_open()
    with patch("builtins.open", m):
        write_env_file(mock_info.variables)
    m.assert_called_once_with(f"{WRITE_PATH}/.env", "w")
    m().writelines.assert_called_with(lines)


def test_creates_valid_config() -> None:
    config_files = create_configs(mock_info.services)
    config.load(ConfigDetails("working_dir", config_files))  # validation
    assert config_files == mock_config_files


def test_write_yaml_files() -> None:
    m = mock_open()
    with patch("builtins.open", m):
        write_yaml(mock_config_files)
    assert m.call_args_list == [
        call(f"{WRITE_PATH}/{c.filename}.yml", "w") for c in mock_config_files
    ]
