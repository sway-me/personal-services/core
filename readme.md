#  Personal Services Core

![](https://gitlab.com/sway-me/personal-services/core/badges/development/pipeline.svg)

## sequence
![](./uml/sequence.svg)
## Source Tree
  
```
src
├── core
│     └── __init__.py
│     └── setup_stacks.py
│     └── deploy_stacks.py
├── clients
│     ├── json
│     │     └── __init__.py
│     │     └── backup.py
│     ├── sqlalchemy
│     │     └── __init__.py
│     ├── __init__.py
├── init__.py
├── ui
│     ├── cli
│     │     └── __init__.py
│     │     └── main.py
│     │     └── get_services.py
│     │     └── get_secrets.py
└── utils.py
```




<p style="font-size: 12px; font-style: italic;">This project was seeded from <a href="https://gitlab.com/Resister/python-starter/">here</a> </p>
