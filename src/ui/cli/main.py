import sys

from PyInquirer import prompt

from src.core.models import RouterInfo
from src.core.setup_services import setup_services
from src.ui.cli.get_info import get_info

initial_question = [
    {
        "type": "confirm",
        "name": "is_ok",
        "message": """During this wizard you will be prompted for:

    ✓ Services you wish to install
    ✓ The domain you purchased from namecheap
    ✓ Namecheap username and API key
    ✓ Your Router make and model

Feel free to cancel and gather the required info before installation.          
Continue?""",
    },
]

print("")
initial_answer = prompt(initial_question)
if not initial_answer["is_ok"]:
    print(
        """
    No prob. Please visit:
        https://docs.beta.swayme.xyz/personal-services/getting-started/
    for more info
    """
    )
    sys.exit()
info = get_info()
result = setup_services(info)


def respond(router_info: RouterInfo, personal_domain: str) -> str:
    return f"""✅ Successfully Installed!
    
1) However, you still need to forward ports 80 and 443. Instructions available at:
        
        https://portforward.com/{str(router_info).replace('.', '/').lower()}/

2) Then open your domain phone:

        https://{personal_domain} 

3) Click on the three dot menu under th icon and select 'app link'
4) Go to https://docs.beta.swayme.xyz/mobile/configuration for details on how to configure your apps
    """


if result:
    response = respond(info.router_info, personal_domain=info.variables.personal_domain)
    print("")
    print(response)
