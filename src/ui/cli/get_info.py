from typing import List, Dict
from PyInquirer import prompt

from src.core.models import Service, Variables, Secrets, RouterInfo, Info

# TODO: turn questions into json - reuse json from docs
# TODO: write proper validations
services_question = [
    {
        "type": "checkbox",
        "qmark": "",
        "message": "Personal Services",
        "name": "services",
        "choices": [
            {
                "name": "Traefik 🚦 (29k⭐): Access services outside of local network",
                "disabled": "Required",
                "checked": True,
            },
            {
                "name": "Bitwarden 🔐 (3.7k⭐): Manage passwords, secrets and tokens",
                "disabled": "Required",
                "checked": True,
            },
            {"name": "Funkwhale 🐋 : Publish and stream other people's music library"},
        ],
    },
]

variable_questions = [
    {
        "type": "input",
        "name": "personal_email",
        "message": "What's the root recovery email for all your data (protonmail recommended)?",
    },
    {
        "type": "input",
        "name": "personal_domain",
        "message": "What is the  personal domain you got from namecheap?",
    },
    {
        "type": "input",
        "name": "namecheap_username",
        "message": "What is your namecheap username?",
    },
]

router_questions = [
    {
        "type": "list",
        "message": "What is the make of your router? e.g Arris, Motorola, Netgear, etc",
        "name": "make",
        "choices": ["netgear", "arris"],
    },
    {
        "type": "list",
        "name": "model",
        "message": "What is the model of your netgear router?",
        "choices": ["r6300"],
        "when": lambda answers: answers["make"] == "netgear",
    },
    {
        "type": "list",
        "name": "model",
        "message": "What is the model of your arris router?",
        "choices": ["dg3450a"],
        "when": lambda answers: answers["make"] == "arris",
    },
]

hide_question = [
    {
        "type": "confirm",
        "qmark": " 👁 ",
        "name": "hide",
        "message": "Would you like to hide your secret info while you type it in the terminal?",
    }
]


def secret_questions(hide: bool) -> List[Dict[str, str]]:
    question_type = "password" if hide else "input"
    return [
        {
            "type": question_type,
            "name": "namecheap_api_key",
            "message": "What is you Namecheap API key?",
        }
    ]


generate_question = [
    {
        "type": "list",
        "name": "generate",
        "message": "Would you like Sway to automatically generate and populate"
        " all passwords in password manager?",
        "choices": ["Yes. Sounds good.", "Nah. I'll do it myself"],
    }
]


def services() -> List[Service]:
    print("")
    answers = prompt(services_question)["services"]
    service_strings = [answer.split()[0].lower().rstrip(":") for answer in answers]
    return [Service[service] for service in service_strings]


def variables() -> Variables:
    print("")
    answers = prompt(variable_questions)
    return Variables(**answers)


def secrets() -> Secrets:
    print("")
    hide = prompt(hide_question)["hide"]
    answers = prompt(secret_questions(hide))
    generate = bool("Yes" in prompt(generate_question)["generate"])
    return Secrets(**answers, generate=generate)


def router_info() -> RouterInfo:
    print("")
    router_answers = prompt(router_questions)
    make = router_answers["make"]
    model = router_answers["model"]
    return RouterInfo[make].value[model]


def get_info() -> Info:
    return Info(
        services=services(),
        variables=variables(),
        secrets=secrets(),
        router_info=router_info(),
    )
