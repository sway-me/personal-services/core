from os import environ, mkdir
from os.path import join, dirname, exists
from typing import List, Dict
import json
import yaml

# TODO: Prune config_validation
from src.core.config_validation.config.config import ConfigFile
from src.core.deploy_stacks import deploy_stacks
from src.core.models import Variables, Info, Service

WRITE_PATH = f"{environ.get('HOME')}/.sway"

VERSIONS = {"bitwarden": "1.20.0", "funkwhale": "1.1.1"}

options_path = join(dirname(__file__), "options.json")
options = json.load(open(options_path))

for k, v in options.items():
    v["image"] = f'{v["image"]}:{VERSIONS[k]}'


def _create_content(service: str) -> Dict[str, object]:
    opts = options[service]
    return {
        "services": {
            service: {
                "image": opts["image"],
                "environment": opts["environment"],
                "volumes": opts["volumes"],
                "labels": [
                    f"traefik.http.routers.{service}.rule=Host(`{service}.$PERSONAL_DOMAIN`)",
                    f"traefik.http.routers.{service}.entrypoints=websecure",
                    f"traefik.http.routers.{service}.tls.certresolver=le",
                    f'traefik.http.services.{service}.loadbalancer.server.port={opts["port"]}',
                ],
            },
        }
    }


def create_configs(services: List[Service]) -> List[ConfigFile]:
    service_strings = [service.value for service in services]
    return [
        ConfigFile(f"{service_string}.yml", _create_content(service_string))
        for service_string in service_strings
    ]


def write_env_file(variables: Variables) -> None:
    var_dict = variables.__dict__
    if not exists(WRITE_PATH):
        mkdir(WRITE_PATH)
    with open(f"{WRITE_PATH}/.env", "w") as h:
        h.writelines([f"{key.upper()}={val}\n" for key, val in var_dict.items()])


def write_yaml(configs: List[ConfigFile]) -> None:
    for config in configs:
        with open(f"{WRITE_PATH}/{config.filename}.yml", "w") as file:
            file.write(yaml.dump(config.config))


def setup_services(info: Info) -> bool:
    write_env_file(info.variables)
    configs = create_configs(info.services)
    write_yaml(configs)
    deploy_stacks(info.services, info.secrets)
    return True
