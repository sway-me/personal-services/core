from dataclasses import dataclass
from enum import Enum
from typing import List


class Service(Enum):
    bitwarden = "bitwarden"
    funkwhale = "funkwhale"


class Netgear(Enum):
    r6300 = "r6300"


class Arris(Enum):
    dg3450a = "dg3450a"


class RouterInfo(Enum):
    netgear = Netgear
    arris = Arris


# TODO: create validator ABC that supports NewType
#  https://github.com/python/mypy/issues/3200
@dataclass
class Secrets:
    namecheap_api_key: str
    generate: bool


@dataclass
class Variables:
    personal_domain: str
    personal_email: str
    namecheap_username: str


@dataclass
class Info:
    services: List[Service]
    variables: Variables
    secrets: Secrets
    router_info: RouterInfo
